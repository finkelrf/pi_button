## Installing the dependencies
```
sudo apt install python3 python3-pip -y
sudo pip3 install RPi.GPIO
```
## Install pi button service 
```
sudo cp pi_button.py /usr/bin/pi_button.py
sudo chmod +x /usr/bin/pi_button.py

sudo cp pi_button.service /etc/systemd/system/pi_button.service
sudo chmod 644 /etc/systemd/system/pi_button.service

sudo systemctl start pi_button
sudo systemctl enable pi_button
```
