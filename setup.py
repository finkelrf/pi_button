import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pi_button", # Replace with your own username
    version="0.0.1",
    author="Rafael Finkelstein",
    author_email="finkel.rf@gmail.com",
    description="A library for handling a push button",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/finkelrf/pi_button.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Linux",
    ],
    install_requires=[
        'RPi.GPIO',
    ]
)