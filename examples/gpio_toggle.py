import subprocess
import pi_button
import RPi.GPIO as GPIO

RELAY_PIN = 4
def long_press_cb(pi_button_dict):
    print('Short press callback example')

def short_press_cb(pi_button_dict):
    print('Long press callback example')

if __name__ == "__main__":
    pb = PiButton(3)
    pb.set_short_press_callback(short_press_cb)
    pb.set_long_press_callback(long_press_cb)

    pb.watch_button()