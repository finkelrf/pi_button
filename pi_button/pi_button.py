#!/usr/bin/env python3

import time
import signal
import sys
import subprocess
import threading
import RPi.GPIO as GPIO

def signal_handler(sig, frame):
    GPIO.cleanup()
    sys.exit(0)

# class BlinkThread(threading.Thread):
#     def __init__(self, period, duration = None):
#         threading.Thread.__init__(self)
#         self.on = True
#         self.period = period
#         self.duration = duration
#         self.start_time = time.time()

#     def run(self):
#         state = 0
#         while self.on:
#             state = not state
#             GPIO.output(LED_PIN, state)
#             time.sleep(self.period)
#             if self.duration != None and time.time() - self.start_time >= self.duration:
#                 self.on = False


#     def off(self):
#         self.on = False
#         GPIO.output(LED_PIN, GPIO.LOW)
    
class PiButton():
    def __init__(self, pb_GPIO):
        signal.signal(signal.SIGINT, signal_handler)
        self.button_GPIO = pb_GPIO
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.button_GPIO, GPIO.IN)
        self.long_press_callback = lambda: None
        self.short_press_callback = lambda: None

    def set_long_press_callback(self, cb):
        self.long_press_callback = cb

    def set_short_press_callback(self, cb):
        self.short_press_callback = cb

    def wait_for_button_press(self):
        # Add a timeout to be able to capture SIGINT. Good for testing.
        while(GPIO.wait_for_edge(self.button_GPIO, GPIO.FALLING, timeout=500) == None):
            pass
        print("Pressed")
        start = time.time()
        time.sleep(0.2)

        while GPIO.input(self.button_GPIO) == GPIO.LOW:
            time.sleep(0.01)
        length = time.time() - start
        print(length)

        return length


    def watch_button(self):
        while True:
            time_pressed = self.wait_for_button_press()

            if time_pressed < 1:
                self.short_press_callback(self.__dict__)
            elif time_pressed > 3:
                self.long_press_callback(self.__dict__)


